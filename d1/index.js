const express = require("express");
// Mongoose is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating our database

const mongoose = require("mongoose");


const app = express()
const port = 3001;

// MongoDB Connection 
// Connecting to MongoDB atlas
mongoose.connect("mongodb+srv://brands27:nevermore27@cluster0.eteec.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	// always put this when connecting in mongoDB
	useNewUrlparser: true,
	useUnifiedTopology: true
})

// set notification for conenction success or failure
let db = mongoose.connection;
// if error occurd, output in the console

db.on("error", console.error.bind(console, "connection error"))

// but if the conenction is successful output this in the console
db.once("open", () => console.log("We're connected to the cloud database."))

app.use(express.json());
app.use(express.urlencoded({ extended:true }))


// Mongoose Schemas

// Schema determine the structure of the documents to be written in the database it acts asa a blueprints to our data

// We will use schema() constructor of the Mongoose module to create a new Schema object

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
})

// everytime you crate a schema you will make a model

const Task = mongoose.model("Task", taskSchema)

// Models are what allows us to gain access to methods that will perform crud funtions

// Models must be in singular form and capitalized following the MVC approach for naming conventions

// firstparameter of the mongoose model method indicates the collection in where store the data

// second parameter is used to specify the Schema/blueprint

// Routes

// Create a new task

/*
Business Logic
1. Add a functionality to check if there are duplicates tasks
	-if the task already exists, return there is a duplicate
	-if the task doesn't exist, we can add it in the database
2. the task data will  be comming from the request's body
3. Create new task object with properties that we need
4. then save the data
*/


app.post("/tasks", (req, res) =>{
	// Check if there are duplicate
	// findOne() is a mongoose method that acts similar to "fiind".
	// it returns the first document that matches thhe search criteria
	Task.findOne( {name: req.body.name}, (error, result) => {
		// if a document was found and the documnent's name matches the information sent cia clientpostman
		if(result !== null && result.name == req.body.name){
			// return a mmessage to a client/postman
			return res.send("Duplicate task found!")
		}else{
			// if no document was found
			// create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// if there are errors in saving 
				if(saveErr){
					// will print any error found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr)
				}else{
					// no error found while creating the document
					// Return a sttatus code of 201 for successful cretion
					return res.status(201).send("New Task Created")
				}
			})
		}



	} )
})


/*
	Retrieving all data
	Business logic
	1.Retrieve all the documents using the find()
	2.If an error is encountered, print the error
	3.If no errors are found, send a success status back to the client and return an array of documents

	*/

app.get("/tasks", (req, res) =>{
	// setting {} will return all the data
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err)
		} else{
			return res.status(200).json({
				data: result
			})
		}
	})
})



// ACTIVITY

/*
	Register a use (Business Logic)
1. Find if there are duplicater user
	if user already exist, we return an error
	if user doesn't exist, we add it in the database
		if the user name and password are both not blank
			if blank send response "BOTH username and password must be provided"
			-Both condtion has been met create a new object 
			-Save the new object
				-if error, return an error message
				-else, response a status 201 and "New user Registered"

*/


const userSchema = new mongoose.Schema({
	username: String,
	password: String

})

const User = mongoose.model("User", userSchema)

app.post("/signup", (req, res) =>{

	User.findOne( {username: req.body.username}, (error, result) => {

		if(result !== null && result.username == req.body.username){
			return res.send("Username already exists")

		}
		else if(req.body.username == ''  && req.body.password == ''){
			
			return res.send("Please input BOTH username and password.")
		}
		else{
	
			let newUser = new User({
				username: req.body.username,
				password: req.body.password

			})

			newUser.save((saveErr, savedTask) => {
			
				if(saveErr){
	
					return console.error(saveErr)
				}else{

					return res.status(201).send("New User Created")
				}
			})
		}



	} )

})

app.get("/signup", (req, res) =>{

	User.find({}, (err, result) => {
		if(err){
			return console.log(err)
		} else{
			return res.status(200).json({
				data: result
			})
			newUser.save()
		}
	})
})


app.listen(port,()=>console.log(`Server is running at port ${port}`))